<?php
include 'inc/koneksi.php';

$id= @$_GET['id'];
$a= @$_GET['page'];
?>

<fieldset>
    

    <?php
    $id= @$_GET['id'];
    $sql = mysqli_query($conn,"SELECT s.id_incident,s.status,s.nama_pelanggan,s.nmr_internet,s.no_telepon,s.alamat,s.contact_person,s.tanggal_open,s.tanggal_close,s.tx,s.rx,
    s.closed_by,s.k_kendala, s.k_closed,s.material,s.teknisi, d.kantor 
    from t_semua s, t_daerah d
     WHERE s.id_incident = d.id_incident
     AND d.id_incident ='$id'") or die (mysqli_error($conn));
    $data = mysqli_fetch_array($sql);
        
    ?>
    
    <legend class="border">Edit Data Pelanggan <?php echo $data['id_incident']; ?></legend>
    <form action ="" method="post">
        <table>
            
                <td>ID Incident</td>
                <td>:</td>
                <td><input type="text" name="id_incident" value ="<?php echo $data['id_incident']; ?>" readonly></td> 

                <td>&nbsp&nbsp&nbsp</td>

                <td>Tx</td>
                <td>:</td>
                <td><input type="text" name="tx" value ="<?php echo $data['tx']; ?>"></td> 
            </tr>

            <tr>
                <!-- <td>Status</td>
                <td>:</td>
                <td><input type="text" name="status" value =""  >
                </td>  -->
                <td>Status</td>
            	<td>:</td>
            	<td><select name="status" id="status">
					<option value="Open" <?php if($data['status'] == "Open"){echo 'selected="selected"';}?>>Open</option>
					<option value="Close" <?php if($data['status'] == "Close"){echo 'selected="selected"';}?>>Close</option>
					<option value="Pending" <?php if($data['status'] == "Pending"){echo 'selected="selected"';}?>>Pending</option>
				</select></td>

                <td>&nbsp&nbsp&nbsp</td>

                <td>Rx</td>
                <td>:</td>
                <td><input type="text" name="rx" value ="<?php echo $data['rx']; ?>"></td>

            </tr>

            <tr>
                <td>Nama Pelanggan</td>
                <td>:</td>
                <td><input type="text" name="nama_pelanggan" value ="<?php echo $data['nama_pelanggan']; ?>"></td> 
            
                <td>&nbsp&nbsp&nbsp</td>
                
                <td>Closed By</td>
                <td>:</td>
                <td><input type="text" name="closed_by" value ="<?php echo $data['closed_by']; ?>"></td>
                
                
            </tr>

            <tr>
                <td>Nomor Internet</td>
                <td>:</td>
                <td><input type="text" name="nmr_internet" value ="<?php echo $data['nmr_internet']; ?>"></td> 

                <td>&nbsp&nbsp&nbsp</td>
                
                <td>Kendala</td>
                <td>:</td>
                <td><select name="k_kendala" id="kendala">
					<option value="Pelanggan Menolak" <?php if($data['k_kendala']=="Pelanggan Menolak"){echo 'selected="selected"';}?>>Pelanggan Menolak</option>
					<option value="Cabut Layanan" <?php if($data['k_kendala']=="Cabut Layanan"){echo 'selected="selected"';}?>>Cabut Layanan</option>
					<option value="Rukos" <?php if($data['k_kendala']=="Rukos"){echo 'selected="selected"';}?>>Rukos</option>
					<option value="Ukuran Balik Jelek" <?php if($data['k_kendala']=="Ukuran Balik Jelek"){echo 'selected="selected"';}?>>Ukuran Balik Jelek</option>
					<option value="Cuaca" <?php if($data['k_kendala']=="Cuaca"){echo 'selected="selected"';}?>>Cuaca</option>
					<option value="Redaman ODP Tinggi" <?php if($data['k_kendala']=="Redaman ODP Tinggi"){echo 'selected="selected"';}?>>Redaman ODP Tinggi</option>
					<option value="Isolir" <?php if($data['k_kendala']=="Isolir"){echo 'selected="selected"';}?>>Isolir</option>
					<option value="Alamat Tidak Ketemu" <?php if($data['k_kendala']=="Alamat Tidak Ketemu"){echo 'selected="selected"';}?>>Alamat Tidak Ketemu</option>
					<option value="Kabel Sulit DiJangkau" <?php if($data['k_kendala']=="Kabel Sulit DiJangkau"){echo 'selected="selected"';}?>>Kabel Sulit DiJangkau</option>
					<option value="Lingkungan" <?php if($data['k_kendala']=="Lingkungan"){echo 'selected="selected"';}?>>Lingkungan</option>
					<option value="Splicer" <?php if($data['k_kendala']=="Splicer"){echo 'selected="selected"';}?>>Splicer</option>
					<option value="Ada Passive" <?php if($data['k_kendala']=="Ada Passive"){echo 'selected="selected"';}?>>Ada Passive</option>
					<option value="Tarikan Jauh" <?php if($data['k_kendala']=="Tarikan Jauh"){echo 'selected="selected"';}?>>Tarikan Jauh</option>
				</select></td>
				
                
            </tr>

			<tr>
                <td>Nomor Telepon</td>
                <td>:</td>
                <td><input type="text" name="no_telepon" value ="<?php echo $data['no_telepon']; ?>"></td> 

                <td>&nbsp&nbsp&nbsp</td>

                <td>Closed</td>
                <td>:</td>
                <td><input type="text" name="k_closed" value ="<?php echo $data['k_closed']; ?>"></td>
                
            </tr>
            
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td><input type="text" name="alamat" value ="<?php echo $data['alamat']; ?>"></td>

                <td>&nbsp&nbsp&nbsp</td>

                <td>Material</td>
                <td>:</td>
                <td><select name="material" id="materials">
					<option value="Ganti DC 50M" <?php if($data['material'] == "Ganti DC 50M"){echo 'selected="selected"';}?>>Ganti DC 50M</option>
					<option value="Ganti DC 75M" <?php if($data['material'] == "Ganti DC 75M"){echo 'selected="selected"';}?>>Ganti DC 75M</option>
					<option value="Ganti DC 100M"<?php if($data['material'] == "Ganti DC 100M"){echo 'selected="selected"';}?>>Ganti DC 100M</option>
					<option value="Ganti DC 150M"<?php if($data['material'] == "Ganti DC 150M"){echo 'selected="selected"';}?>>Ganti DC 150M</option>
					<option value="Ganti DC 200M"<?php if($data['material'] == "Ganti DC 200M"){echo 'selected="selected"';}?>>Ganti DC 200M</option>
					<option value="Ganti DC 250M"<?php if($data['material'] == "Ganti DC 250M"){echo 'selected="selected"';}?>>Ganti DC 250M</option>
					<option value="Ganti PC 2M"  <?php if($data['material'] == "Ganti PC 2M"){echo 'selected="selected"';}?>>Ganti PC 2M</option>
					<option value="Ganti PC 5M"  <?php if($data['material'] == "Ganti PC 5M"){echo 'selected="selected"';}?>>Ganti PC 5M</option>
					<option value="Ganti PC 10M" <?php if($data['material'] == "Ganti PC 10M"){echo 'selected="selected"';}?>>Ganti PC 10M</option>
					<option value="Ganti PC 15M" <?php if($data['material'] == "Ganti PC 15M"){echo 'selected="selected"';}?>>Ganti PC 15M</option>
					<option value="Ganti PC 20M" <?php if($data['material'] == "Ganti PC 20M"){echo 'selected="selected"';}?>>Ganti PC 20M</option>
					<option value="Ganti Modem"  <?php if($data['material'] == "Ganti Modem"){echo 'selected="selected"';}?>>Ganti Modem</option>
					<option value="Ganti Adaptor Conn" <?php if($data['material'] == "Ganti Adaptor Conn"){echo 'selected="selected"';}?>>Ganti Adaptor/Conn</option>
					<option value="Ganti PS 1-4" <?php if($data['material'] == "Ganti PS 1-4"){echo 'selected="selected"';}?>>Ganti PS 1:4</option>
					<option value="Ganti PS 1-8" <?php if($data['material'] == "Ganti PS 1-8"){echo 'selected="selected"';}?>>Ganti PS 1:8</option>
					<option value="Splice Ulang" <?php if($data['material'] == "Splice Ulang"){echo 'selected="selected"';}?>>Splice Ulang</option>
					<option value="Splice Ulang SOC" <?php if($data['material'] == "Splice Ulang SOC"){echo 'selected="selected"';}?>>Splice Ulang SOC</option>
					<option value="Pembersihan Connect" <?php if($data['material'] == "Pembersihan Conncet"){echo 'selected="selected"';}?>>Pembersihan Connect</option>
					<option value="Perapihan Kabel" <?php if($data['material'] == "Perapihan Kabel"){echo 'selected="selected"';}?>>Perapihan Kabel</option>
					<option value="Ukur Massal" <?php if($data['material'] == "Ukur Massal"){echo 'selected="selected"';}?>>Ukur Massal</option>
					<option value="Replace Pasive" <?php if($data['material'] == "Replace Pasive"){echo 'selected="selected"';}?>>Replace Passive</option>
					<option value="Ganti PC Adaptor" <?php if($data['material'] == "Ganti PC Adaptor"){echo 'selected="selected"';}?>>Ganti PC & Adaptor</option>
					<option value="Tarik Ulang" <?php if($data['material'] == "Tarik Ulang"){echo 'selected="selected"';}?>>Tarik Ulang</option>
					<option value="Konfigurasi Ulang" <?php if($data['material'] == "Konfigurasi Ulang"){echo 'selected="selected"';}?>>Konfigurasi Ulang</option>
				</select></td>

                
                
            </tr>

            <tr>
                <td>Contact Person</td>
                <td>:</td>
                <td><input type="text" name="contact_person" value ="<?php echo $data['contact_person']; ?>"></td> 

                
                <td>&nbsp&nbsp&nbsp</td>

                <td>Teknisi</td>
                <td>:</td>
                <td><input type="text" name="teknisi" value ="<?php echo $data['teknisi']; ?>"></td> 

            </tr>


            <tr>
                <td>STO</td>
            	<td>:</td>
            	<td><input type="text" name="kantor" value ="<?php echo $data['kantor'];?>" readonly></td>                 
            </tr>

            <tr>
                <td>Tanggal Update</td>
                <td>:</td>
                <!-- <td><input type="date" name="tanggal" value =" "></td> -->
                <td><input type="date" name="tanggal_open" value = "<?php echo date('Y-m-d'); ?>"></td>
            </tr>
            

            <tr>
                <td>Tanggal Close</td>
                <td>:</td>
                <td><input type="date" name="tanggal_close" value ="<?php echo $data['tanggal_close']; ?>"></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td><br/><input type="submit" class="btn btn-danger btn-xs" name="edit" value="Edit" /> <input type="button" class="btn btn-primary btn-xs" value="Batal" onclick="history.back(-1)" /></td>
            </tr>
        </table>
    </form>

    <?php
    $id = @$_POST['id_incident'];
    $status = @$_POST['status'];
    $nama_pelanggan = @$_POST['nama_pelanggan'];
    $nmr_internet = @$_POST['nmr_internet'];
    $no_telepon = @$_POST['no_telepon'];
    $alamat = @$_POST['alamat'];
    $contact_person = @$_POST['contact_person'];
    $tanggal = @$_POST['tanggal_open'];
    $tx = @$_POST['tx'];
    $rx = @$_POST['rx'];
    $closed_by = @$_POST['closed_by'];
    $kendala = @$_POST['k_kendala'];
    $closed = @$_POST['k_closed'];
    $material = @$_POST['material'];
    $teknisi = @$_POST['teknisi'];
    $tanggal_closed = @$_POST['tanggal_close'];


    $edit_pelanggan = @$_POST['edit'];

    if($edit_pelanggan){
        if($status == ""|| $nama_pelanggan == ""|| $nmr_internet == ""|| $no_telepon == ""|| $alamat==""|| $contact_person == ""|| $tanggal == ""|| $tx == ""|| $rx == ""|| $closed_by == ""|| $kendala == ""|| $closed == ""|| $material == ""|| $teknisi == ""){
            ?> 
            <script type="text/javascript">
            alert("Inputan tidak boleh ada yang kosong");
            </script>
            <?php
        } else{
            $update = mysqli_query($conn,"UPDATE t_semua set id_incident = '$id', status='$status', nmr_internet = '$nmr_internet', nama_pelanggan = '$nama_pelanggan', no_telepon = '$no_telepon', alamat = '$alamat', contact_person = '$contact_person', tanggal_open = '$tanggal', tanggal_close = '$tanggal_closed', tx = '$tx', rx = '$rx', closed_by = '$closed_by', k_kendala = '$kendala', k_closed = '$closed', material = '$material', teknisi = '$teknisi' where id_incident = '$id'") or die (mysqli_error($conn));
            
            ?> 
            <script type="text/javascript">
            alert("Data berhasil diedit");
            window.location.href="?page=<?=$a?>&id=<?=$id?>";
            </script>
            // require './assets/PHPMailer/PHPMailerAutoload.php';

            // $mail = new PHPMailer;
            //             // Konfigurasi SMTP
            // $mail->isSMTP();
            // $mail->Host = 'smtp.gmail.com';
            // $mail->SMTPAuth = true;
            // $mail->Username = 'kertayasa1997@gmail.com';
            // $mail->Password = '210810denpasar';
            // $mail->SMTPSecure = 'tls';
            // $mail->Port = 587;

            // $mail->setFrom('kertayasa1997@gmail.com', 'Notif Edit Pelanggan');
            // $mail->addReplyTo('kertayasa1997@gmail.com', 'Telkom Akses Magelang');

            // // Menambahkan penerima
            // $mail->addAddress('har633mgl@gmail.com');

            // // Subjek email
            // $mail->Subject = 'Email Notif Edit Pelanggan';

            // // Mengatur format email ke HTML
            // $mail->isHTML(true);

            // // Konten/isi email
            // $mailContent = "<div style='background-color: #eeeeef; padding: 50px 0; '>    
            //             <div style='max-width:640px; margin:0 auto; '>  <div style='color: #fff; text-align: center; background-color:#E53935; padding: 30px; border-top-left-radius: 3px; border-top-right-radius: 3px; margin: 0;'>
            //                 <h1>Edit Pelanggan</h1> 
            //             </div> 
            //                     <div style='padding: 20px; background-color: rgb(255, 255, 255);'>
   
            //                     <p style=''><span style='color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;'>Detail Pelanggan: </span><br><br></p>
                                
            //                     <p style=''><span style='color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;'>Port : $port</span><br><br></p>
            //                       <p style=''><span style='color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;'>ODC : $odc</span><br><br></p>
            //                     <p style=''><span style='color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;'>ODP : $odp</span><br><br></p>
            //                     <p style=''><span style='color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;'>Pekerjaan : $pekerjaan</span><br><br></p>

            //                     <p style=''><span style='color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;'>No Internet : $nmr_internet</span><br><br></p>

            //                     <p style=''><span style='color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;'>Nama : $nama</span><br><br></p>

            //                     <p style=''><span style='color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;'>No Telp : $nmr_tlp</span><br><br></p>

            //                     <p style=''><span style='color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;'>Qr Code : $qr_code</span><br><br></p>

            //                      <p style=''><span style='color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;'>Tanggal Update : $tanggal_update</span><br><br></p>
                              
            //                     <p style=''><span style='color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;'><br></span></p>           
                                     
            //                     <p style=''><span style='color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;'><br></span></p>
            //                     <p style=''><span style='color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;'>If you don't want to accept this notification, simply ignore this email.</span><br><br></p>            
            //                     <p style='color: rgb(85, 85, 85); font-size: 14px;'>Telkom Akses Magelang</p>        
            //                 </div>    
            //             </div>
            //         </div>";
            // $mail->Body = $mailContent;

            // // Kirim email
            // if(!$mail->send()){
            //     echo 'Pesan tidak dapat dikirim.';
            //     echo 'Mailer Error: ' . $mail->ErrorInfo;
            // }
            
            <?php
        }
    }
    ?>
</fieldset>