<?php
include 'inc/koneksi.php';
?>

<fieldset>
	<legend class="border">Tambah Tiket</legend>

	<form action ="" method="post">
		<table>
			
            <tr>
            	<td class="form" >ID Incident :</td>
            	<td>:</td>
            	<td><input id="idIncident" type="text" name="id_incident"></td>

                <td>&nbsp&nbsp&nbsp</td>

            	<td>Tx</td>
            	<td>:</td>
            	<td><input type="text" name="tx"></td> 
            </tr>

            <tr>
            	<td>Status</td>
            	<td>:</td>
            	<td><select name="status" id="status">
            		<option >------PILIH STATUS-----</option>
					<option value="Open">Open</option>
					<option value="Close">Close</option>
					<option value="Pending">Pending</option>
				</select></td>  
                
                <td>&nbsp&nbsp&nbsp</td>

            	<td>Rx</td>
            	<td>:</td>
            	<td><input type="text" name="rx"></td>
            </tr>

            <tr>
            	<td>Nama Pelanggan</td>
            	<td>:</td>
            	<td><input type="text" name="nama_pelanggan"></td> 

                <td>&nbsp&nbsp&nbsp</td>
				
				<td>Closed By</td>
                <td>:</td>
                <td><input type="text" name="closed_by" value="-"></td>

				
            </tr>

            <tr>
            	<td>Nomor Internet</td>
            	<td>:</td>
            	<td><input type="text" name="nmr_internet"></td> 

                <td>&nbsp&nbsp&nbsp</td>

				<td>Kendala</td>
                <td>:</td>
				<td><select name="k_kendala" id="kendala">
					<option >----------</option>
					<option value="Pelanggan Menolak">Pelanggan Menolak</option>
					<option value="Cabut Layanan">Cabut Layanan</option>
					<option value="Rukos">Rukos</option>
					<option value="Ukuran Balik Jelek">Ukuran Balik Jelek</option>
					<option value="Cuaca">Cuaca</option>
					<option value="Redaman ODP Tinggi">Redaman ODP Tinggi</option>
					<option value="Isolir">Isolir</option>
					<option value="Alamat Tidak Ketemu">Alamat Tidak Ketemu</option>
					<option value="Kabel Sulit DiJangkau">Kabel Sulit DiJangkau</option>
					<option value="Lingkungan">Lingkungan</option>
					<option value="Splicer">Splicer</option>
					<option value="Ada Passive">Ada Passive</option>
					<option value="Tarikan Jauh">Tarikan Jauh</option>
				</select></td>
				
                
            </tr>

            <tr>
            	<td>Nomor Telepon</td>
            	<td>:</td>
            	<td><input type="text" name="no_telepon"></td> 

                <td>&nbsp&nbsp&nbsp</td>

				<td>Closed</td>
                <td>:</td>
                <td><input type="text" name="k_closed" value="-"></td>
            
			</tr>	
            <tr>
            	<td>Alamat</td>
            	<td>:</td>
            	<td><input type="text" name="alamat"></td> 

                <td>&nbsp&nbsp&nbsp</td>
				
				<td>Material</td>
                <td>:</td>
				<td><select name="material" id="materials">
					<option >---------</option>
					<option value="Ganti DC 50M">Ganti DC 50M</option>
					<option value="Ganti DC 75M">Ganti DC 75M</option>
					<option value="Ganti DC 100M">Ganti DC 100M</option>
					<option value="Ganti DC 150M">Ganti DC 150M</option>
					<option value="Ganti DC 200M">Ganti DC 200M</option>
					<option value="Ganti DC 250M">Ganti DC 250M</option>
					<option value="Ganti PC 2M">Ganti PC 2M</option>
					<option value="Ganti PC 5M">Ganti PC 5M</option>
					<option value="Ganti PC 10M">Ganti PC 10M</option>
					<option value="Ganti PC 15M">Ganti PC 15M</option>
					<option value="Ganti PC 20M">Ganti PC 20M</option>
					<option value="Ganti Modem">Ganti Modem</option>
					<option value="Ganti Adaptor Conn">Ganti Adaptor/Conn</option>
					<option value="Ganti PS 1-4">Ganti PS 1:4</option>
					<option value="Ganti PS 1-8">Ganti PS 1:8</option>
					<option value="Splice Ulang">Splice Ulang</option>
					<option value="Splice Ulang SOC">Splice Ulang SOC</option>
					<option value="Pembersihan Connect">Pembersihan Connect</option>
					<option value="Perapihan Kabel">Perapihan Kabel</option>
					<option value="Ukur Massal">Ukur Massal</option>
					<option value="Replace Pasive">Replace Passive</option>
					<option value="Ganti PC Adaptor">Ganti PC & Adaptor</option>
					<option value="Tarik Ulang">Tarik Ulang</option>
					<option value="Konfigurasi Ulang">Konfigurasi Ulang</option>
				</select></td>
				<!-- <td><input type="text" name="material"></td> -->
            

            </tr>

            <tr>
            	<td>Contact Person</td>
            	<td>:</td>
            	<td><input type="text" name="contact_person"></td> 
				

                <td>&nbsp&nbsp&nbsp</td>
				
				<td>Teknisi</td>
                <td>:</td>
				<td><select name="teknisi" id="">
					<option>---------</option>
					<option value="99170157">99170157</option>
					<option value="97170297">97170297</option>
					<option value="97170294">97170294</option>
					<option value="96170188">96170188</option>
					<option value="93157529">93157529</option>
					<option value="93155481">93155481</option>
					<option value="91157362">91157362</option>
					<option value="90157544">90157544</option>
					<option value="87157541">87157541</option>
					<option value="17960866">17960866</option>
					<option value="18910073">18910073</option>
					<option value="18920069">18920069</option>
					<option value="18970231">18970231</option>
					<option value="18960224">18960224</option>
					<option value="18940292">18940292</option>
				</select></td>
                <!-- <td><input type="text" name="teknisi"></td> -->
                
				
            </tr>

            <tr>
            	<td>STO</td>
            	<td>:</td>
            	<td><select name="kantor" id="kota">
            		<option >------PILIH WILAYAH-----</option>
					<option value="Magelang">Magelang</option>
					<option value="Gombong">Gombong</option>
					<option value="Karanganyar">Karanganyar</option>
					<option value="Mertoyudan">Mertoyudan</option>
					<option value="Kebumen">Kebumen</option>
					<option value="Sawitan">Sawitan</option>
					<option value="Kutowinangun">Kutowinangun</option>
					<option value="Muntilan">Muntilan</option>
					<option value="Kutoarjo">Kutoarjo</option>
					<option value="Parakan">Parakan</option>
					<option value="Purworejo">Purworejo</option>
					<option value="Temanggung">Temanggung</option>
					<option value="Wonosobo">Wonosobo</option>
				</select></td> 

                <td>&nbsp&nbsp&nbsp</td>

                
            </tr>

            <tr>
            	<td>Tanggal</td>
            	<td>:</td>
            	<td><input id="dateNow" type="date" name="tanggal_open" value = "<?php echo date('Y-m-d'); ?>"></td> 
            </tr>
			
            <tr>
            	<td>Tanggal Close</td>
            	<td>:</td>
            	<td><input type="date" name="tanggal_close" disabled></td> 
            </tr>

            <tr>
            	<td></td>
            	<td></td>
            	<td><br/><input type="submit" name="tambah"  class="btn btn-danger btn-xs"  value="Tambah" /> <input type="button"  class="btn btn-primary btn-xs"  value="Batal" onclick="history.back(-1)" /></td>
            </tr>
		</table>

        
	</form>

	<?php
    $id = @$_POST['id_incident'];
    $status = @$_POST['status'];
    $nama = @$_POST['nama_pelanggan'];
    $nmr = @$_POST['nmr_internet'];
    $no_telepon = @$_POST['no_telepon'];
    $alamat = @$_POST['alamat'];
    $contact = @$_POST['contact_person'];
    $tanggal = @$_POST['tanggal_open'];
    $tanggal_close = @$_POST['tanggal_close'];
    $kantor = @$_POST['kantor'];
    $tx = @$_POST['tx'];
	$rx = @$_POST['rx'];
	$closed_by = @$_POST['closed_by'];
    $kendala = @$_POST['k_kendala'];
    $closed = @$_POST['k_closed'];
    $material = @$_POST['material'];
    $teknisi = @$_POST['teknisi'];

	$sqlCheck = mysqli_query($conn,"SELECT * FROM t_semua WHERE id_incident = '$id'");

	if(mysqli_fetch_array($sqlCheck) > 0)
	{
		?>
		<script type="text/javascript">
			alert("ID incident sudah ada");
			window.location.href="?page=semuatiket";
			</script> 
			
			<?php
	}


	$tambah_tiket = @$_POST['tambah'];

	if($tambah_tiket){
		if($status==""|| $nama==""|| $nmr==""|| $no_telepon==""|| $alamat==""|| $contact==""||$kantor==""|| $tanggal==""|| $tx==""|| $rx==""|| $closed_by==""|| $kendala==""|| $closed==""|| $material==""|| $teknisi==""){
			?> 
			<script type="text/javascript">
			alert("Inputan tidak boleh ada yang kosong");
			</script>
			<?php
		} else{
			
			$sql = mysqli_query($conn,"INSERT into t_semua (id_incident,status,nmr_internet,nama_pelanggan,no_telepon,alamat,contact_person,tanggal_open,tanggal_close,tx,rx,closed_by,k_kendala,k_closed,material,teknisi) 
														values('$id' , '$status' , '$nmr' , '$nama' , '$no_telepon' , '$alamat' , '$contact' , '$tanggal' , '$tanggal_close' , '$tx' , '$rx' , '$closed_by' , '$kendala' , '$closed' , '$material' , '$teknisi')") or die (mysqli_error($conn));  
			$sql1 = mysqli_query($conn,"INSERT into t_daerah (kantor,id_incident) values ('$kantor','$id')") or die (mysqli_error($conn));
			
			$sqlPrepareTeknisi = mysqli_query($conn,"SELECT nik FROM t_teknisi WHERE nama = '$teknisi'");

			if($sqlPrepareTeknisi){
				$data = mysqli_fetch_array($sqlPrepareTeknisi);
				$nikTeknisi = $data['nik'];
				$sqlTiketTeknisi = mysqli_query($conn,"INSERT INTO tiket_teknisi (nik,nama,id_incident,status) VALUES ('$teknisi', '$teknisi', '$id', '$status')") or die (mysqli_error($conn));

			}
			
			// require 'assets/PHPMailer/PHPMailerAutoload.php';

			// $mail = new PHPMailer;
			// 			// Konfigurasi SMTP
			// $mail->isSMTP();
			// $mail->Host = 'smtp.gmail.com';
			// $mail->SMTPAuth = true;
			// $mail->Username = 'kertayasa1997@gmail.com';
			// $mail->Password = '210810denpasar';
			// $mail->SMTPSecure = 'tls';
			// $mail->Port = 587;

			// $mail->setFrom('kertayasa1997@gmail.com', 'Notif Tambah ODC');
			// $mail->addReplyTo('kertayasa1997@gmail.com', 'Telkom Akses Magelang');

			// // Menambahkan penerima
			// $mail->addAddress('har633mgl@gmail.com');

			// // Subjek email
			// $mail->Subject = 'Email Notif Tambah ODC';

			// // Mengatur format email ke HTML
			// $mail->isHTML(true);

			// // Konten/isi email
			// $mailContent = "<div style='background-color: #eeeeef; padding: 50px 0; '>    
            //             <div style='max-width:640px; margin:0 auto; '>  <div style='color: #fff; text-align: center; background-color:#E53935; padding: 30px; border-top-left-radius: 3px; border-top-right-radius: 3px; margin: 0;'>
            //                 <h1>Tambah ODC</h1> 
            //             </div> 
            //                     <div style='padding: 20px; background-color: rgb(255, 255, 255);'>
   
            //                     <p style=''><span style='color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;'>Detail ODC: </span><br><br></p>
            //                     <p style=''><span style='color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;'>Kantor : $kantor</span><br><br></p>
            //                     <p style=''><span style='color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;'>ODC : $odc</span><br><br></p>
                              
                              

            //                     <p style=''><span style='color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;'><br></span></p>           
                                     
            //                     <p style=''><span style='color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;'><br></span></p>
            //                     <p style=''><span style='color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;'>If you don't want to accept this notification, simply ignore this email.</span><br><br></p>            
            //                     <p style='color: rgb(85, 85, 85); font-size: 14px;'>Telkom Akses Magelang</p>        
            //                 </div>    
            //             </div>
            //         </div>";
			// $mail->Body = $mailContent;

			// // Kirim email
			// if(!$mail->send()){
			//     echo 'Pesan tidak dapat dikirim.';
			//     echo 'Mailer Error: ' . $mail->ErrorInfo;
			// }

			?> 
			<script type="text/javascript">
			alert("Tambah data Tiket berhasil");
			window.location.href="?page=semuatiket";
			</script>
			<?php
		}
	}
	?>
</fieldset>