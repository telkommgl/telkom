<?php
include 'inc/koneksi.php';
$_SESSION["isLogin"] = "";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<style type="text/css">
body{
	font-family: arial;
	font-size: 14px;
	background-color: #222;
}

#utama{
	width: 300px;
	margin: 0 auto;
	margin-top: 12%;
}

#judul{
	padding: 15px;
	text-align: center;
	color: #fff;
	font-size: 20px;
	background-color: orangered;
	border-top-right-radius: 10px;
	border-top-left-radius: 10px;
	border-bottom: 3px solid orangered;
}

#inputan{
	background-color: #ccc;
	padding: 20px;
	border-bottom-right-radius: 10px;
	border-bottom-left-radius: 10px;
}

input{
	padding: 10px;
	border: 0;
}

.lg{
	width: 240px;
}

.btn{
	background-color: orangered;
	border-radius: 10px;
	color: #fff;
}
.btn:hover{
	background-color: orangered;
	cursor: pointer; 
}
</style>
</head>
<body>

    <div id="utama">
	    <div id="judul">
		    Halaman Login
	    </div>

	    <div id="inputan">
	        <form action="" method="post">
		        <div>
			        <input type="text" name="user" placeholder="Username" class="lg"/>
		        </div>
                <div style="margin-top: 10px">
                    <input type="password" name="pass" placeholder="Password" class="lg" />
                </div>
                <div style="margin-top: 10px">
                    <input type="submit" name="login" value="Login" class="btn" />
                </div>
	        </form>

            <?php
                $user =@$_POST['user'];
                $pass = @$_POST['pass'];
                $login = @$_POST['login'];

                if($login){
                    if($user == "" || $pass == ""){
                        ?> <script type="text/javascript">alert("Username / password tidak boleh kosong");</script>  <?php
                    } else {
                        $sql = mysqli_query($conn,"SELECT * FROM t_login where username = '$user' and pass = '$pass'");

                        if(mysqli_num_rows($sql)>0){
							$_SESSION["isLogin"] = "true";
                            header("location: index.php");
                        } else {
                            echo "Login gagal";
                        }
                    }
                }
            ?>
	    </div>
    </div>

</body>
</html>