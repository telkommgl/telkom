<?php
        include "inc/koneksi.php";
        if($_SESSION["isLogin"] == "")
        {
            header("location: login.php");
        }

        ?>

<!DOCTYPE html>
<html>
    <head>
        <title>Data TIKET Magelang</title>

        <link rel="stylesheet" href="assets/datatables/css/bootstrap.min.css">  
        <link rel="stylesheet" href="assets/datatables/css/dataTables.bootstrap.css">
        <link rel="stylesheet" href="assets/datatables/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="assets/jquery-ui.css" type = "text/css"/>
        <link rel="stylesheet" href="css/index_css.css"    type = "text/css">
        
    </head>
    <body>

        

    <div id="canvas">
        <div id="header" style="color:black">
        
        <span id = "headerTitle">Data TIKET Magelang</span>
        <img style="float:right" src="assets/logo.png">
        </div>

    <div id="menu">
            <ul>
                <li class="utama"><a href="index.php"><img src="assets/home.png"> Home</a>
                <li class="utama"><a href="?page=semuatiket">Lihat Semua Tiket</a></li>
                <li class="utama"><a href="?page=tambahtiket">Tambah Tiket</a></li>
                <li class="utama"><a href="?page=import">Import</a></li>
                <li class="utama"><a href="?page=export">Export</a></li>
                <li class="utama"><a href="information/informationtiket.php">Informasi</a></li>

               <!--  <li class="utama"><a href="">Convert<img src="assets/arrow.png"></a>
                    <ul>
                        <li><a href="?page=import">Import</a></li>
                        <li><a href="?page=export">Export</a></li>
                    </ul>
                </li> -->
                <li class="utama" style="float:right;"><a href = "inc/logout.php"> <img src="assets/out.png">  Logout</a></li>
        </div>  

        <div id="isi">
        <?php
        $page = @$_GET['page'];
        $action = @$_GET['action'];
        if($page=="MG"){
            if($action == ""){
                include "HalamanKantor/MG.php";
            } else if($action == "lihat"){
                include "HalamanMG/Tiket_MG.php";
            } else if($action == "edito"){
                include "tiket/editpelanggan.php";
            } else if($action == "hapuso"){
                include "HalamanMG/HapusoMG.php";
            } else if($action == "lihato"){
                include "HalamanMG/LihatoMG.php";
            }
        } else if($page=="MTY"){
            if($action == ""){
                include "HalamanKantor/MTY.php";
            } else if($action == "lihat"){
                include "HalamanMTY/Tiket_MTY.php";
            }else if($action == "edito"){
                include "tiket/editpelanggan.php";
            } else if($action == "hapuso"){
                include "HalamanMTY/HapusoMTY.php";
            } else if($action == "lihato"){
                include "HalamanMTY/LihatoMTY.php";
            }
        } else if($page=="MNT"){
            if($action == ""){
                include "HalamanKantor/MNT.php";
            } else if($action == "lihat"){
                include "HalamanMNT/Tiket_MNT.php";
            } else if($action == "edito"){
                include "tiket/editpelanggan.php";
            } else if($action == "hapuso"){
                include "HalamanMNT/HapusoMNT.php";
            } else if($action == "lihato"){
                include "HalamanMNT/LihatoMNT.php";
            }
        } else if($page=="SWN"){
            if($action == ""){
                include "HalamanKantor/SWN.php";
            } else if($action == "lihat"){
                include "HalamanMKD/Tiket_SWN.php";
            } else if($action == "edito"){
                include "tiket/editpelanggan.php";
            } else if($action == "hapuso"){
                include "HalamanMKD/HapusoMKD.php";
            } else if($action == "lihato"){
                include "HalamanSWN/LihatoSWN.php";
            }
        } else if($page=="PWR"){
            if($action == ""){
                include "HalamanKantor/PWR.php";
            } else if($action == "lihat"){
                include "HalamanPWR/Tiket_PWR.php";
            } else if($action == "edito"){
                include "tiket/editpelanggan.php";
            } else if($action == "hapuso"){
                include "HalamanPWR/HapusoPWR.php";
            } else if($action == "lihato"){
                include "HalamanPWR/LihatoPWR.php";
            }
        } else if($page=="KTA"){
            if($action == ""){
                include "HalamanKantor/KTA.php";
            } else if($action == "lihat"){
                include "HalamanKTA/Tiket_KTA.php";
            } else if($action == "edito"){
                include "tiket/editpelanggan.php";
            } else if($action == "hapuso"){
                include "HalamanKTA/HapusoKTA.php";
            } else if($action == "lihato"){
                include "HalamanKTA/LihatoKTA.php";
            }
        } else if($page=="KWN"){
            if($action == ""){
                include "HalamanKantor/KWN.php";
            } else if($action == "lihat"){
                include "HalamanKWN/Tiket_KWN.php";
            } else if($action == "edito"){
                include "tiket/editpelanggan.php";
            } else if($action == "hapuso"){
                include "HalamanKWN/HapusoKWN.php";
            } else if($action == "lihato"){
                include "HalamanKWN/LihatoKWN.php";
            }
        } else if($page=="KM"){
            if($action == ""){
                include "HalamanKantor/KM.php";
            } else if($action == "lihat"){
                include "HalamanKM/Tiket_KM.php";
            } else if($action == "edito"){
                include "tiket/editpelanggan.php";
            } else if($action == "hapuso"){
                include "HalamanKM/HapusoKM.php";
            } else if($action == "lihato"){
                include "HalamanKM/LihatoKM.php";
            }
        } else if($page=="KA"){
            if($action == ""){
                include "HalamanKantor/KA.php";
            } else if($action == "lihat"){
                include "HalamanKA/Tiket_KA.php";
            } else if($action == "edito"){
                include "tiket/editpelanggan.php";
            } else if($action == "hapuso"){
                include "HalamanKA/HapusoKA.php";
            } else if($action == "lihato"){
                include "HalamanKA/LihatoKA.php";
            }
        } else if($page=="GB"){
            if($action == ""){
                include "HalamanKantor/GB.php";
            } else if($action == "lihat"){
                include "HalamanGB/Tiket_GB.php";
            } else if($action == "edito"){
                include "tiket/editpelanggan.php";
            } else if($action == "hapuso"){
                include "HalamanGB/HapusoGB.php";
            } else if($action == "lihato"){
                include "HalamanGB/LihatoGB.php";
            }
        } else if($page=="TMG"){
            if($action == ""){
                include "HalamanKantor/TMG.php";
            } else if($action == "lihat"){
                include "HalamanTMG/Tiket_TMG.php";
            } else if($action == "edito"){
                include "tiket/editpelanggan.php";
            } else if($action == "hapuso"){
                include "HalamanTMG/HapusoTMG.php";
            } else if($action == "lihato"){
                include "HalamanTMG/LihatoTMG.php";
            }
        } else if($page=="PRN"){
            if($action == ""){
                include "HalamanKantor/PRNN.php";
            } else if($action == "lihat"){
                include "HalamanPRN/Tiket_PRN.php";
            } else if($action == "edito"){
                include "tiket/editpelanggan.php";
            } else if($action == "hapuso"){
                include "HalamanPRN/HapusoPRN.php";
            } else if($action == "lihato"){
                include "HalamanPRN/LihatoPRN.php";
            }
        } else if($page=="WNS"){
            if($action == ""){
                include "HalamanKantor/WNS.php";
            } else if($action == "lihat"){
                include "HalamanWNS/Tiket_WNS.php";
            } else if($action == "edito"){
                include "tiket/editpelanggan.php";
            } else if($action == "hapuso"){
                include "HalamanWNS/HapusoWNS.php";
            } else if($action == "lihato"){
                include "HalamanWNS/LihatoWNS.php";
            }

        }else if($page == "semuatiket"){
            if($action == ""){
                include "semuatiket.php";   
            }
            else if($action == "editpelanggan"){
                include "tiket/editpelanggan.php";
            }else if($action == "hapuspelanggan"){
                include "tiket/hapuspelanggan.php";
            }else if($action == "tambahtiket"){
                include "tiket/tambahtiket.php";
            }
           
        }else if($page == "close"){
            if($action == ""){
                include "close.php";
            }
            else if($action == "editpelanggan"){
                include "tiket/editpelanggan.php";
            }
        } else if($page == "open"){
            if($action == ""){
                include "open.php";
            }
        }else if($page == "pending"){
            if($action == ""){
                include "pending.php";
            }
        }else if($page == "view"){
            if($action == ""){
                include "tiket/view.php";
            }
            
        }else if($page == "tambahtiket"){
            include "tiket/tambahtiket.php";
        }else if($page == "export"){
            include "export.php";
        }else if($page == "import"){
            include "import.php";
        }else if($page == "cetak_excel"){
            if($action == ""){
                include "cetak_excel.php";
            }
        }else if($page=="cari"){
            include "cari.php";
        }else if($page ==""){
            include "awal.php";
        }
        ?>
        </div>

        <div>
        <footer class="section footer-classic context-dark bg-image " style="background: #2d3246;">
        <div class="container">
          <div class="row row-30 test">
            <!-- <div class="col-md-4 col-xl-5 kosong"></div> -->
            <div class="col-md-4 kontak">
              <h5>Contacts</h5>
              <dl class="contact-list">
                <dt>Address:</dt>
                <dd>Jl. Semarang - Yogyakarta No.10, Tidar Sel., Magelang Sel., Kota Magelang, Jawa Tengah 56125</dd>
              </dl>
              <dl class="contact-list">
                <dt>Email:</dt>
                <dd><a href="mailto:#">CustomerCare@telkom.co.id</a></dd>
              </dl>
              <dl class="contact-list">
                <dt>Telpon:</dt>
                <dd> (kode Area) 147</dd>
              </dl>
            </div>

             <div class="col-md-4 col-xl-3 " id="kotak1">
              <h5>Links</h5>
              <ul class="nav-list">
                <li><a href="https://indihome.co.id">Indihome</a></li>
                <li><a href="https://www.telkom.co.id/servlet/tk/about/id_ID/tkahomepage/halaman-telkom-indonesia.html">Telkom indonesia</a></li>
                <li><a href="https://www.google.com/maps/place/Telkom+Akses/@-7.4965653,110.1903319,13z/data=!4m12!1m6!3m5!1s0x2e7a85efad56bc8f:0xf648d021b55a8078!2stelkom!8m2!3d-7.4760669!4d110.2166603!3m4!1s0x0:0xd62a4f2abc67d464!8m2!3d-7.5003553!4d110.2253258">Lokasi</a></li>
              </ul>
            </div>

          </div>
        </div>
        <div id="footer">
            © Copyright 2018 -  WITEL MAGELANG
        
            </div>
      </footer>


        </div> 
        
    </div>

    <script src="assets/datatables/js/jquery-1.11.1.min.js"></script>
            <script src="assets/datatables/js/bootstrap.min.js"></script>
            <script src="assets/datatables/js/jquery.dataTables.min.js"></script>
            <script src="assets/datatables/js/dataTables.bootstrap.js"></script>


    <script src="assets/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/jquery-ui.js" type="text/javascript"></script>  
            <script type="text/javascript">
                $(function() {
                    $('#example1').dataTable({
    "language": {
        "search": "Cari:"
    }
    });

                });
            </script>



    <script>
    var dolar_js = jQuery.noConflict();
    dolar_js(function() {
        dolar_js( "#datepicker" ).datepicker({
                dateFormat:'yyyy/mm/dd',
                changeMonth: true,
                changeYear: true
            });
    });
    </script>

    </body>

</html>