
<html>
<head>

    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Data Daerah</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://code.jquery.com/jquery-latest.min.js"></script>
    <link rel="stylesheet" href="../assets/bootstrap-4.1.3-dist/css/bootstrap.min.css" type = "text/css">
    <script src="../assets/bootstrap-4.1.3-dist/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="../css/maininformation.css" type = "text/css">

    <?php 
    include_once('../inc/koneksi.php');

    if( null !== @$_POST['type_daerah']){
        $daerah = @$_POST['type_daerah'];    
    } else {
        $daerah = 'magelang';
    }
    

    $sqlOpen = mysqli_query($conn,"SELECT t_daerah.kantor, t_semua.id_incident , t_semua.status 
                                   FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident
                                   WHERE kantor = '$daerah' AND status = 'open'");
    $openCount = mysqli_num_rows($sqlOpen);

    $sqlClose = mysqli_query($conn,"SELECT t_daerah.kantor, t_semua.id_incident , t_semua.status 
                                    FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident
                                    WHERE kantor = '$daerah' AND status = 'close'");
    $closeCount = mysqli_num_rows($sqlClose);

    $sqlPending = mysqli_query($conn,"SELECT t_daerah.kantor, t_semua.id_incident , t_semua.status 
                                      FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident
                                      WHERE kantor = '$daerah' AND status = 'pending'");
    $pendingCount = mysqli_num_rows($sqlPending);
    
    $sqlMenolak = mysqli_query($conn,"SELECT t_daerah.kantor, t_semua.id_incident , t_semua.status, t_semua.k_kendala 
                                      FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident
                                      WHERE kantor = '$daerah' AND k_kendala = 'Pelanggan Menolak'");
    $menolakCount = mysqli_num_rows($sqlMenolak);

    $sqlCabut = mysqli_query($conn,"SELECT t_daerah.kantor, t_semua.id_incident , t_semua.status, t_semua.k_kendala 
                                      FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident
                                      WHERE kantor = '$daerah' AND k_kendala = 'Cabut Layanan'");
    $cabutCount = mysqli_num_rows($sqlCabut);

    $sqlRukos = mysqli_query($conn,"SELECT t_daerah.kantor, t_semua.id_incident , t_semua.status, t_semua.k_kendala 
                                      FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident
                                      WHERE kantor = '$daerah' AND k_kendala = 'Rukos'");
    $rukosCount = mysqli_num_rows($sqlRukos);

    $sqlUkuranBalik = mysqli_query($conn,"SELECT t_daerah.kantor, t_semua.id_incident , t_semua.status, t_semua.k_kendala 
                                      FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident
                                      WHERE kantor = '$daerah' AND k_kendala = 'Ukuran Balik Jelek'");
    $ukuranBalikCount = mysqli_num_rows($sqlUkuranBalik);

    $sqlCuaca = mysqli_query($conn,"SELECT t_daerah.kantor, t_semua.id_incident , t_semua.status, t_semua.k_kendala 
                                      FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident
                                      WHERE kantor = '$daerah' AND k_kendala = 'Cuaca'");
    $cuacaCount = mysqli_num_rows($sqlCuaca);

    $sqlODP = mysqli_query($conn,"SELECT t_daerah.kantor, t_semua.id_incident , t_semua.status, t_semua.k_kendala 
                                      FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident
                                      WHERE kantor = '$daerah' AND k_kendala = 'Redaman ODP Tinggi'");
    $odpCount = mysqli_num_rows($sqlODP);

    $sqlIsolir = mysqli_query($conn,"SELECT t_daerah.kantor, t_semua.id_incident , t_semua.status, t_semua.k_kendala 
                                      FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident
                                      WHERE kantor = '$daerah' AND k_kendala = 'Isolir'");
    $isolirCount = mysqli_num_rows($sqlIsolir);

    $sqlAlamat = mysqli_query($conn,"SELECT t_daerah.kantor, t_semua.id_incident , t_semua.status, t_semua.k_kendala 
                                      FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident
                                      WHERE kantor = '$daerah' AND k_kendala = 'Alamat Tidak Ketemu'");
    $alamatCount = mysqli_num_rows($sqlAlamat);

    $sqlKabel = mysqli_query($conn,"SELECT t_daerah.kantor, t_semua.id_incident , t_semua.status, t_semua.k_kendala 
                                      FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident
                                      WHERE kantor = '$daerah' AND k_kendala = 'Kabel Sulit DiJangkau'");
    $kabelCount = mysqli_num_rows($sqlKabel);

    $sqlLingkungan = mysqli_query($conn,"SELECT t_daerah.kantor, t_semua.id_incident , t_semua.status, t_semua.k_kendala 
                                      FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident
                                      WHERE kantor = '$daerah' AND k_kendala = 'Lingkungan'");
    $lingkunganCount = mysqli_num_rows($sqlLingkungan);

    $sqlSplicer = mysqli_query($conn,"SELECT t_daerah.kantor, t_semua.id_incident , t_semua.status, t_semua.k_kendala 
                                      FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident
                                      WHERE kantor = '$daerah' AND k_kendala = 'Splicer'");
    $splicerCount = mysqli_num_rows($sqlSplicer);

    $sqlPassive = mysqli_query($conn,"SELECT t_daerah.kantor, t_semua.id_incident , t_semua.status, t_semua.k_kendala 
                                      FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident
                                      WHERE kantor = '$daerah' AND k_kendala = 'Ada Passive'");
    $passiveCount = mysqli_num_rows($sqlPassive);

    $sqlTarikan = mysqli_query($conn,"SELECT t_daerah.kantor, t_semua.id_incident , t_semua.status, t_semua.k_kendala 
                                      FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident
                                      WHERE kantor = '$daerah' AND k_kendala = 'Tarikan Jauh'");
    $tarikanCount = mysqli_num_rows($sqlTarikan);
    
    
    ?>

    <script type='text/javascript'>

    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var chartStatus = new google.visualization.DataTable();
        chartStatus.addColumn('string', 'Status');
        chartStatus.addColumn('number', 'Jumlah');
        chartStatus.addRows([
            ['Open', <?php echo $openCount?>],
            ['Pending', <?php echo $pendingCount?>],
            ['Close', <?php echo $closeCount?>]
        ]);

        var chartKendala = new google.visualization.DataTable();
        chartKendala.addColumn('string', 'Kendala');
        chartKendala.addColumn('number', 'Jumlah');
        chartKendala.addRows([
            ['Pelanggan Menolak', <?php echo $menolakCount?>],
            ['Cabut Layanan', <?php echo $cabutCount?>],
            ['Rukos', <?php echo $rukosCount?>],
            ['Ukuran Balik Jelek', <?php echo $ukuranBalikCount?>],
            ['Cuaca', <?php echo $cuacaCount?>],
            ['Redaman ODP Tinggi', <?php echo $odpCount?>],
            ['Isolir', <?php echo $isolirCount?>],
            ['Alamat Tidak Ketemu', <?php echo $alamatCount?>],
            ['Kabel Sulit DiJangkau', <?php echo $kabelCount?>],
            ['Lingkungan', <?php echo $lingkunganCount?>],
            ['Splicer', <?php echo $splicerCount?>],
            ['Ada Passive', <?php echo $passiveCount?>],
            ['Tarikan Jauh', <?php echo $tarikanCount?>]
        ]);

        var chartColumnStatus = google.visualization.arrayToDataTable([
            ['Status', 'Jumlah', { role: 'style' }],
            ['Open', <?php echo $openCount?>, 'blue'],            // RGB value
            ['Pending', <?php echo $pendingCount?>, 'red'],            // English color name
            ['Close', <?php echo $closeCount?>, 'yellow'],
        // ['Platinum', 21.45, 'color: #e5e4e2' ], // CSS-style declaration
        ]);

        var chartColumnKendala = google.visualization.arrayToDataTable([
            ['Status', 'Jumlah', { role: 'style' }],
            ['Pelanggan Menolak', <?php echo $menolakCount?>, 'blue'],
            ['Cabut Layanan', <?php echo $cabutCount?>, 'red'],
            ['Rukos', <?php echo $rukosCount?>, 'yellow'],
            ['Ukuran Balik Jelek', <?php echo $ukuranBalikCount?>, 'green'],
            ['Cuaca', <?php echo $cuacaCount?>,'pink'],
            ['Redaman ODP Tinggi', <?php echo $odpCount?>,'black'],
            ['Isolir', <?php echo $isolirCount?>,'grey'],
            ['Alamat Tidak Ketemu', <?php echo $alamatCount?>,'brown'],
            ['Kabel Sulit DiJangkau', <?php echo $kabelCount?>,'cyan'],
            ['Lingkungan', <?php echo $lingkunganCount?>,'orange'],
            ['Splicer', <?php echo $splicerCount?>,'#42f4d4'],
            ['Ada Passive', <?php echo $passiveCount?>,'#c141f4'],
            ['Tarikan Jauh', <?php echo $tarikanCount?>,'#d3f441']
        // ['Platinum', 21.45, 'color: #e5e4e2' ], // CSS-style declaration
        ]);

        // Set chart options
        var optionStatus = {'title':'Chart Status Tiket <?php echo $daerah ?> ',
                        'width':600,
                        'height':300};
        var optionKendala = {'title':'Chart Kendala <?php echo $daerah ?> ',
                        'width':600,
                        'height':300};
        var optionColumnKendala = {'title':'Chart Kendala <?php echo $daerah ?> ',
                        'width':900,
                        'height':300};                        

        // Instantiate and draw our chart, passing in some options.
        // var chart = new google.visualization.PieChart(document.getElementById('pie_chart_div'));
        // var chartDaerah = new google.visualization.PieChart(document.getElementById('pc_daerah'));
        // var chartColumn = new google.visualization.ColumnChart(document.getElementById('column_chart_div'));
        // chart.draw(data, options);
        // chartDaerah.draw(chartKendala, optionsDaerah);
        // chartColumn.draw(dataColumnChart, options);
    
            var pcStatus = new google.visualization.PieChart(document.getElementById('pcStatus'));
            var pcKendala = new google.visualization.PieChart(document.getElementById('pcKendala'));
            var columnStatus = new google.visualization.ColumnChart(document.getElementById('columnStatus'));
            var columnKendala = new google.visualization.ColumnChart(document.getElementById('columnKendala'));

            pcStatus.draw(chartStatus, optionStatus);
            pcKendala.draw(chartKendala, optionKendala);
            columnStatus.draw(chartColumnStatus, optionStatus);
            columnKendala.draw(chartColumnKendala, optionColumnKendala);

    }

</script>
    
</head>
<body>
<div class="canvas">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="informationtiket.php">Statistik Tiket</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="../index.php">Home</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="informationtiket.php">Tiket by Bulan</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="informationdaerah.php">Tiket Daerah</a>
        </li>
         <li class="nav-item">
            <a class="nav-link" href="informationteknisi.php">Tiket Teknisi</a>
        </li>
        </ul>
    </div>
    </nav>

    <form action="informationdaerah.php" method="post">
        <div class="input-group mb-3 bg-light">
        <div class="input-group-prepend">
            <label class="input-group-text" for="inputGroupSelect01">Show By : </label>
        </div>
        <select name = "type_daerah" class="custom-select" id="infTiket">
            <option selected value="magelang">Magelang</option>
            <option value="gombong">Gombong</option>
            <option value="karanganyar">Karanganyar</option>
            <option value="mertoyudan">Mertoyudan</option>
            <option value="kebumen">Kebumen</option>
            <option value="sawitan">Sawitan</option>
            <option value="kutowinangun">Kutowinangun</option>
            <option value="muntilan">Muntilan</option>
            <option value="kutoarjo">Kutoarjo</option>
            <option value="parakan">Parakan</option>
            <option value="purworejo">Purworejo</option>
            <option value="temanggung">Temanggung</option>
            <option value="wonosobo">Wonosobo</option>
        </select>
        <button type="submit" class="btn btn-default">Submit</button>
        </div> 
    </form>
    <table>
    <tr>
    <td><div class="posisi" id = "pcStatus"></div></td>
    </tr>
    <tr>
    <td><div class="posisi" id = "columnStatus"></div></td>
    </tr>
    <tr>
    <td><div class="posisi" id = "pcKendala"></div></td>
    </tr>
    <tr>
    <td><div class="posisi1" id = "columnKendala"></div></td>
    </tr>
    </table>
    
    </div>
</body>
</html>