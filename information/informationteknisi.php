<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Teknisi</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://code.jquery.com/jquery-latest.min.js"></script>
    <link rel="stylesheet" href="../assets/bootstrap-4.1.3-dist/css/bootstrap.min.css" type = "text/css">
    <script src="../assets/bootstrap-4.1.3-dist/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="../css/maininformation.css" type = "text/css">

    <?php 
    include_once('../inc/koneksi.php');

    $teknisi = "Default";

    if(null !== @$_POST['type_teknisi']){
        $teknisi = @$_POST['type_teknisi'];    
    } else {
        $teknisi = '99170157';
    }


    $sqlOpen = mysqli_query($conn, "SELECT * FROM tiket_teknisi WHERE nama = '$teknisi' AND status = 'open'");
    $openCount = mysqli_num_rows($sqlOpen);

    $sqlClose = mysqli_query($conn, "SELECT * FROM tiket_teknisi WHERE nama = '$teknisi' AND status = 'close'");
    $closeCount = mysqli_num_rows($sqlClose);

    $sqlPending = mysqli_query($conn, "SELECT * FROM tiket_teknisi WHERE nama = '$teknisi' AND status = 'pending'");
    $pendingCount = mysqli_num_rows($sqlPending);

    $sqlMenolak = mysqli_query($conn,"SELECT t_semua.teknisi, t_semua.k_kendala, t_semua.id_incident , t_semua.status 
                                      FROM t_semua NATURAL JOIN tiket_teknisi
                                      WHERE t_semua.teknisi = '$teknisi' AND t_semua.k_kendala = 'Pelanggan Menolak'");
    $menolakCount = mysqli_num_rows($sqlMenolak);

    $sqlCabut = mysqli_query($conn,"SELECT t_semua.teknisi, t_semua.k_kendala, t_semua.id_incident , t_semua.status 
                                      FROM t_semua NATURAL JOIN tiket_teknisi
                                      WHERE t_semua.teknisi = '$teknisi' AND t_semua.k_kendala = 'Cabut Layanan'");
    $cabutCount = mysqli_num_rows($sqlCabut);

    $sqlRukos = mysqli_query($conn,"SELECT t_semua.teknisi, t_semua.k_kendala, t_semua.id_incident , t_semua.status 
                                      FROM t_semua NATURAL JOIN tiket_teknisi
                                      WHERE t_semua.teknisi = '$teknisi' AND t_semua.k_kendala = 'Rukos'");
    $rukosCount = mysqli_num_rows($sqlRukos);

    $sqlUkuranBalik = mysqli_query($conn,"SELECT t_semua.teknisi, t_semua.k_kendala, t_semua.id_incident , t_semua.status 
                                      FROM t_semua NATURAL JOIN tiket_teknisi
                                      WHERE t_semua.teknisi = '$teknisi' AND t_semua.k_kendala = 'Ukuran Balik Jelek'");
    $ukuranBalikCount = mysqli_num_rows($sqlUkuranBalik);

    $sqlCuaca = mysqli_query($conn,"SELECT t_semua.teknisi, t_semua.k_kendala, t_semua.id_incident , t_semua.status 
                                      FROM t_semua NATURAL JOIN tiket_teknisi
                                      WHERE t_semua.teknisi = '$teknisi' AND t_semua.k_kendala = 'Cuaca'");
    $cuacaCount = mysqli_num_rows($sqlCuaca);

    $sqlODP = mysqli_query($conn,"SELECT t_semua.teknisi, t_semua.k_kendala, t_semua.id_incident , t_semua.status 
                                      FROM t_semua NATURAL JOIN tiket_teknisi
                                      WHERE t_semua.teknisi = '$teknisi' AND t_semua.k_kendala = 'Redaman ODP Tinggi'");
    $odpCount = mysqli_num_rows($sqlODP);

    $sqlIsolir = mysqli_query($conn,"SELECT t_semua.teknisi, t_semua.k_kendala, t_semua.id_incident , t_semua.status 
                                      FROM t_semua NATURAL JOIN tiket_teknisi
                                      WHERE t_semua.teknisi = '$teknisi' AND t_semua.k_kendala = 'Isolir'");
    $isolirCount = mysqli_num_rows($sqlIsolir);

    $sqlAlamat = mysqli_query($conn,"SELECT t_semua.teknisi, t_semua.k_kendala, t_semua.id_incident , t_semua.status 
                                      FROM t_semua NATURAL JOIN tiket_teknisi
                                      WHERE t_semua.teknisi = '$teknisi' AND t_semua.k_kendala = 'Alamat Tidak Ketemu'");
    $alamatCount = mysqli_num_rows($sqlAlamat);

    $sqlKabel = mysqli_query($conn,"SELECT t_semua.teknisi, t_semua.k_kendala, t_semua.id_incident , t_semua.status 
                                      FROM t_semua NATURAL JOIN tiket_teknisi
                                      WHERE t_semua.teknisi = '$teknisi' AND t_semua.k_kendala = 'Kabel Sulit DiJangkau'");
    $kabelCount = mysqli_num_rows($sqlKabel);

    $sqlLingkungan = mysqli_query($conn,"SELECT t_semua.teknisi, t_semua.k_kendala, t_semua.id_incident , t_semua.status 
                                      FROM t_semua NATURAL JOIN tiket_teknisi
                                      WHERE t_semua.teknisi = '$teknisi' AND t_semua.k_kendala = 'Lingkungan'");
    $lingkunganCount = mysqli_num_rows($sqlLingkungan);

    $sqlSplicer = mysqli_query($conn,"SELECT t_semua.teknisi, t_semua.k_kendala, t_semua.id_incident , t_semua.status 
                                      FROM t_semua NATURAL JOIN tiket_teknisi
                                      WHERE t_semua.teknisi = '$teknisi' AND t_semua.k_kendala = 'Splicer'");
    $splicerCount = mysqli_num_rows($sqlSplicer);

    $sqlPassive = mysqli_query($conn,"SELECT t_semua.teknisi, t_semua.k_kendala, t_semua.id_incident , t_semua.status 
                                      FROM t_semua NATURAL JOIN tiket_teknisi
                                      WHERE t_semua.teknisi = '$teknisi' AND t_semua.k_kendala = 'Ada Passive'");
    $passiveCount = mysqli_num_rows($sqlPassive);

    $sqlTarikan = mysqli_query($conn,"SELECT t_semua.teknisi, t_semua.k_kendala, t_semua.id_incident , t_semua.status 
                                      FROM t_semua NATURAL JOIN tiket_teknisi
                                      WHERE t_semua.teknisi = '$teknisi' AND t_semua.k_kendala = 'Tarikan Jauh'");
    $tarikanCount = mysqli_num_rows($sqlTarikan);


    ?>

    <script type='text/javascript'>

google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {

    var chartStatus = new google.visualization.DataTable();
    chartStatus.addColumn('string', 'Status');
    chartStatus.addColumn('number', 'Jumlah');
    chartStatus.addRows([
        ['Open', <?php echo $openCount?>],
        ['Pending', <?php echo $pendingCount?>],
        ['Close', <?php echo $closeCount?>]
    ]);

    var chartKendala = new google.visualization.DataTable();
    chartKendala.addColumn('string', 'Kendala');
    chartKendala.addColumn('number', 'Jumlah');
    chartKendala.addRows([
        ['Pelanggan Menolak', <?php echo $menolakCount?>],
        ['Cabut Layanan', <?php echo $cabutCount?>],
        ['Rukos', <?php echo $rukosCount?>],
        ['Ukuran Balik Jelek', <?php echo $ukuranBalikCount?>],
        ['Cuaca', <?php echo $cuacaCount?>],
        ['Redaman ODP Tinggi', <?php echo $odpCount?>],
        ['Isolir', <?php echo $isolirCount?>],
        ['Alamat Tidak Ketemu', <?php echo $alamatCount?>],
        ['Kabel Sulit DiJangkau', <?php echo $kabelCount?>],
        ['Lingkungan', <?php echo $lingkunganCount?>],
        ['Splicer', <?php echo $splicerCount?>],
        ['Ada Passive', <?php echo $passiveCount?>],
        ['Tarikan Jauh', <?php echo $tarikanCount?>]
    ]);

    var chartColumnStatus = google.visualization.arrayToDataTable([
        ['Status', 'Jumlah', { role: 'style' }],
        ['Open', <?php echo $openCount?>, 'blue'],            // RGB value
        ['Pending', <?php echo $pendingCount?>, 'red'],            // English color name
        ['Close', <?php echo $closeCount?>, 'yellow'],
    // ['Platinum', 21.45, 'color: #e5e4e2' ], // CSS-style declaration
    ]);

    var chartColumnKendala = google.visualization.arrayToDataTable([
        ['Status', 'Jumlah', { role: 'style' }],
        ['Pelanggan Menolak', <?php echo $menolakCount?>, 'blue'],
        ['Cabut Layanan', <?php echo $cabutCount?>, 'red'],
        ['Rukos', <?php echo $rukosCount?>, 'yellow'],
        ['Ukuran Balik Jelek', <?php echo $ukuranBalikCount?>, 'green'],
        ['Cuaca', <?php echo $cuacaCount?>,'pink'],
        ['Redaman ODP Tinggi', <?php echo $odpCount?>,'black'],
        ['Isolir', <?php echo $isolirCount?>,'grey'],
        ['Alamat Tidak Ketemu', <?php echo $alamatCount?>,'brown'],
        ['Kabel Sulit DiJangkau', <?php echo $kabelCount?>,'cyan'],
        ['Lingkungan', <?php echo $lingkunganCount?>,'orange'],
        ['Splicer', <?php echo $splicerCount?>,'#42f4d4'],
        ['Ada Passive', <?php echo $passiveCount?>,'#c141f4'],
        ['Tarikan Jauh', <?php echo $tarikanCount?>,'#d3f441']
    // ['Platinum', 21.45, 'color: #e5e4e2' ], // CSS-style declaration
    ]);

    // Set chart options
    var optionStatus = {'title':'Status Tiket Milik <?php echo $teknisi ?> ',
                    'width':600,
                    'height':300};
    var optionKendala = {'title':'Chart Kendala Milik <?php echo $teknisi ?> ',
                    'width':600,
                    'height':300};
    var optionColumnKendala = {'title':'Grafik Kendala Tiket Milik <?php echo $teknisi ?> ',
                    'width':900,
                    'height':300};                        

    // Instantiate and draw our chart, passing in some options.
    // var chart = new google.visualization.PieChart(document.getElementById('pie_chart_div'));
    // var chartDaerah = new google.visualization.PieChart(document.getElementById('pc_daerah'));
    // var chartColumn = new google.visualization.ColumnChart(document.getElementById('column_chart_div'));
    // chart.draw(data, options);
    // chartDaerah.draw(chartKendala, optionsDaerah);
    // chartColumn.draw(dataColumnChart, options);

        var pcStatus = new google.visualization.PieChart(document.getElementById('pcStatus'));
        var pcKendala = new google.visualization.PieChart(document.getElementById('pcKendala'));
        var columnStatus = new google.visualization.ColumnChart(document.getElementById('columnStatus'));
        var columnKendala = new google.visualization.ColumnChart(document.getElementById('columnKendala'));

        pcStatus.draw(chartStatus, optionStatus);
        pcKendala.draw(chartKendala, optionKendala);
        columnStatus.draw(chartColumnStatus, optionStatus);
        columnKendala.draw(chartColumnKendala, optionColumnKendala);

}

</script>

</head>
<body>
    <div class="canvas">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="informationtiket.php">Statistik Tiket</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="../index.php">Home</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="informationtiket.php">Tiket by Bulan</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="informationdaerah.php">Tiket Daerah</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="informationteknisi.php">Tiket Teknisi</a>
        </li>
        </ul>
    </div>
    </nav>

    <form action="informationteknisi.php" method="post">
        <div class="input-group mb-3 bg-light">
        <div class="input-group-prepend">
            <label class="input-group-text" for="inputGroupSelect01">Show By : </label>
        </div>
        <select name = "type_teknisi" class="custom-select" id="infTiket">
                    <option>---PILIH---</option>
                    <option value="99170157">99170157</option>
					<option value="97170297">97170297</option>
					<option value="97170294">97170294</option>
					<option value="96170188">96170188</option>
					<option value="93157529">93157529</option>
					<option value="93155481">93155481</option>
					<option value="91157362">91157362</option>
					<option value="90157544">90157544</option>
					<option value="87157541">87157541</option>
					<option value="17960866">17960866</option>
					<option value="18910073">18910073</option>
					<option value="18920069">18920069</option>
					<option value="18970231">18970231</option>
					<option value="18960224">18960224</option>
					<option value="18940292">18940292</option>
        </select>
        <button type="submit" class="btn btn-default">Submit</button>
        </div> 
    </form>

    <tr>
    <td><div class="posisi" id = "pcStatus"></div></td>
    </tr>
    <tr>   
    <td><div  class="posisi" id = "columnStatus"></div></td>
    </tr>
    <tr>
    <td><div  class="posisi" id = "pcKendala"></div></td>
    </tr>
    <tr>
    <td><div  class="posisi2" id = "columnKendala"></div></td>
    </tr>
    </div>  
</body>
</html>