
<html>
<head>
    <link rel="stylesheet" href="../assets/bootstrap-4.1.3-dist/css/bootstrap.min.css" type = "text/css">
    <script src="../assets/bootstrap-4.1.3-dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://code.jquery.com/jquery-latest.min.js"></script>

    <link rel="stylesheet" href="../css/maininformation.css" type = "text/css">

    <?php 

    include '../inc/koneksi.php';
    $date = date('Y-m');
    $thisYear = date('Y');
    $thisDateMonth = date('F Y');

    switch (@$_POST['type_bulan']) {
        case '01':
            $thisDateMonth = "Januari ".$thisYear;
            break;
        case '02':
            $thisDateMonth = "Februari ".$thisYear;
            break;
        case '03':
            $thisDateMonth = "Maret ".$thisYear;
            break;
        case '04':
            $thisDateMonth = "April ".$thisYear;
            break;
        case '05':
            $thisDateMonth = "Mei ".$thisYear;
            break;
        case '06':
            $thisDateMonth = "Juni ".$thisYear;
            break;
        case '07':
            $thisDateMonth = "Juli ".$thisYear;
            break;
        case '08':
            $thisDateMonth = "Agustus ".$thisYear;
            break;
        case '09':
            $thisDateMonth = "September ".$thisYear;
            break;
        case '10':
            $thisDateMonth = "Oktober ".$thisYear;
            break;
        case '11':
            $thisDateMonth = "November ".$thisYear;
            break;
        case '12':
            $thisDateMonth = "Desember ".$thisYear;
            break;
        default:
            $thisDateMonth = date('F Y');
    }


    if(@$_POST['type_bulan'] != null)
    {
        $var = @$_POST['type_bulan'];
        $date = $thisYear."-".$var;
    }

        $sqlOpenMonth = mysqli_query($conn,"SELECT * FROM t_semua WHERE status = 'open' AND tanggal_open LIKE '$date-%'");
        $openCount = mysqli_num_rows($sqlOpenMonth);

        $sqlCloseMonth =  mysqli_query($conn,"SELECT * FROM t_semua WHERE status = 'close' AND tanggal_open LIKE '$date-%'");
        $closeCount = mysqli_num_rows($sqlCloseMonth);

        $sqlPendingMonth = mysqli_query($conn,"SELECT * FROM t_semua WHERE status = 'pending' AND tanggal_open LIKE '$date-%'");
        $pendingCount = mysqli_num_rows($sqlPendingMonth);

        //BY WILAYAH
        $sqlMagelang = mysqli_query($conn, "SELECT t_daerah.id_incident FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident WHERE t_daerah.kantor = 'Magelang' AND t_semua.tanggal_open LIKE '$date-%'");
        $magelangCount = mysqli_num_rows($sqlMagelang);
        
        $sqlGombong = mysqli_query($conn,"SELECT t_daerah.id_incident FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident WHERE t_daerah.kantor = 'Gombong' AND t_semua.tanggal_open LIKE '$date-%'");
        $gombongCount = mysqli_num_rows($sqlGombong);

        $sqlKaranganyar = mysqli_query($conn, "SELECT t_daerah.id_incident FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident WHERE t_daerah.kantor = 'Karanganyar' AND t_semua.tanggal_open LIKE '$date-%'");
        $karanganyarCount = mysqli_num_rows($sqlKaranganyar);
        
        $sqlMertoyudan = mysqli_query($conn, "SELECT t_daerah.id_incident FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident WHERE t_daerah.kantor = 'Mertoyudan' AND t_semua.tanggal_open LIKE '$date-%'");
        $mertoyudanCount = mysqli_num_rows($sqlMertoyudan);
        
        $sqlKebumen = mysqli_query($conn, "SELECT t_daerah.id_incident FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident WHERE t_daerah.kantor = 'Kebumen' AND t_semua.tanggal_open LIKE '$date-%'");
        $kebumenCount = mysqli_num_rows($sqlKebumen);
        
        $sqlSawitan = mysqli_query($conn, "SELECT t_daerah.id_incident FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident WHERE t_daerah.kantor = 'Sawitan' AND t_semua.tanggal_open LIKE '$date-%'");
        $sawitanCount = mysqli_num_rows($sqlSawitan);
        
        $sqlKutowinangun = mysqli_query($conn, "SELECT t_daerah.id_incident FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident WHERE t_daerah.kantor = 'Kutowinangun' AND t_semua.tanggal_open LIKE '$date-%'");
        $kutowinangunCount = mysqli_num_rows($sqlKutowinangun);

        $sqlMuntilan = mysqli_query($conn, "SELECT t_daerah.id_incident FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident WHERE t_daerah.kantor = 'Muntilan' AND t_semua.tanggal_open LIKE '$date-%'");
        $muntilanCount = mysqli_num_rows($sqlMuntilan);
        
        $sqlKutoarjo = mysqli_query($conn, "SELECT t_daerah.id_incident FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident WHERE t_daerah.kantor = 'Kutoarjo' AND t_semua.tanggal_open LIKE '$date-%'");
        $kutoarjoCount = mysqli_num_rows($sqlKutoarjo);
        
        $sqlParakan = mysqli_query($conn, "SELECT t_daerah.id_incident FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident WHERE t_daerah.kantor = 'Parakan' AND t_semua.tanggal_open LIKE '$date-%'");
        $parakanCount = mysqli_num_rows($sqlParakan);
        
        $sqlPurworejo = mysqli_query($conn, "SELECT t_daerah.id_incident FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident WHERE t_daerah.kantor = 'Purworejo' AND t_semua.tanggal_open LIKE '$date-%'");
        $purworejoCount = mysqli_num_rows($sqlPurworejo);
        
        $sqlTemanggung = mysqli_query($conn, "SELECT t_daerah.id_incident FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident WHERE t_daerah.kantor = 'Temanggung' AND t_semua.tanggal_open LIKE '$date-%'");
        $temanggungCount = mysqli_num_rows($sqlTemanggung);
        
        $sqlWonosobo = mysqli_query($conn, "SELECT t_daerah.id_incident FROM t_daerah INNER JOIN t_semua ON t_daerah.id_incident = t_semua.id_incident WHERE t_daerah.kantor = 'Wonosobo' AND t_semua.tanggal_open LIKE '$date-%'");
        $wonosoboCount = mysqli_num_rows($sqlWonosobo);

        $teknisi1 = mysqli_query($conn,"SELECT tiket_teknisi.status FROM tiket_teknisi INNER JOIN t_semua ON tiket_teknisi.id_incident = t_semua.id_incident WHERE tiket_teknisi.NAMA = '99170157' AND tiket_teknisi.status = 'Close'");
        $teknisi1Count = mysqli_num_rows($teknisi1);

        $teknisi2 = mysqli_query($conn,"SELECT tiket_teknisi.status FROM tiket_teknisi INNER JOIN t_semua ON tiket_teknisi.id_incident = t_semua.id_incident WHERE tiket_teknisi.NAMA = '97170297' AND tiket_teknisi.status = 'Close'");
        $teknisi2Count = mysqli_num_rows($teknisi2);

        $teknisi3 = mysqli_query($conn,"SELECT tiket_teknisi.status FROM tiket_teknisi INNER JOIN t_semua ON tiket_teknisi.id_incident = t_semua.id_incident WHERE tiket_teknisi.NAMA = '97170294' AND tiket_teknisi.status = 'Close'");
        $teknisi3Count = mysqli_num_rows($teknisi3);

        $teknisi4 = mysqli_query($conn,"SELECT tiket_teknisi.status FROM tiket_teknisi INNER JOIN t_semua ON tiket_teknisi.id_incident = t_semua.id_incident WHERE tiket_teknisi.NAMA = '96170188' AND tiket_teknisi.status = 'Close'");
        $teknisi4Count = mysqli_num_rows($teknisi4);

        $teknisi5 = mysqli_query($conn,"SELECT tiket_teknisi.status FROM tiket_teknisi INNER JOIN t_semua ON tiket_teknisi.id_incident = t_semua.id_incident WHERE tiket_teknisi.NAMA = '93157529' AND tiket_teknisi.status = 'Close'");
        $teknisi5Count = mysqli_num_rows($teknisi5);

        $teknisi6 = mysqli_query($conn,"SELECT tiket_teknisi.status FROM tiket_teknisi INNER JOIN t_semua ON tiket_teknisi.id_incident = t_semua.id_incident WHERE tiket_teknisi.NAMA = '93155481' AND tiket_teknisi.status = 'Close'");
        $teknisi6Count = mysqli_num_rows($teknisi6);

        $teknisi7 = mysqli_query($conn,"SELECT tiket_teknisi.status FROM tiket_teknisi INNER JOIN t_semua ON tiket_teknisi.id_incident = t_semua.id_incident WHERE tiket_teknisi.NAMA = '91157362' AND tiket_teknisi.status = 'Close'");
        $teknisi7Count = mysqli_num_rows($teknisi7);

        $teknisi8 = mysqli_query($conn,"SELECT tiket_teknisi.status FROM tiket_teknisi INNER JOIN t_semua ON tiket_teknisi.id_incident = t_semua.id_incident WHERE tiket_teknisi.NAMA = '90157544' AND tiket_teknisi.status = 'Close'");
        $teknisi8Count = mysqli_num_rows($teknisi8);

        $teknisi9 = mysqli_query($conn,"SELECT tiket_teknisi.status FROM tiket_teknisi INNER JOIN t_semua ON tiket_teknisi.id_incident = t_semua.id_incident WHERE tiket_teknisi.NAMA = '87157541' AND tiket_teknisi.status = 'Close'");
        $teknisi9Count = mysqli_num_rows($teknisi9);

        $teknisi10 = mysqli_query($conn,"SELECT tiket_teknisi.status FROM tiket_teknisi INNER JOIN t_semua ON tiket_teknisi.id_incident = t_semua.id_incident WHERE tiket_teknisi.NAMA = '17960866' AND tiket_teknisi.status = 'Close'");
        $teknisi10Count = mysqli_num_rows($teknisi10);

        $teknisi11 = mysqli_query($conn,"SELECT tiket_teknisi.status FROM tiket_teknisi INNER JOIN t_semua ON tiket_teknisi.id_incident = t_semua.id_incident WHERE tiket_teknisi.NAMA = '18910073' AND tiket_teknisi.status = 'Close'");
        $teknisi11Count = mysqli_num_rows($teknisi11);

        $teknisi12 = mysqli_query($conn,"SELECT tiket_teknisi.status FROM tiket_teknisi INNER JOIN t_semua ON tiket_teknisi.id_incident = t_semua.id_incident WHERE tiket_teknisi.NAMA = '18920069' AND tiket_teknisi.status = 'Close'");
        $teknisi12Count = mysqli_num_rows($teknisi12);

        $teknisi13 = mysqli_query($conn,"SELECT tiket_teknisi.status FROM tiket_teknisi INNER JOIN t_semua ON tiket_teknisi.id_incident = t_semua.id_incident WHERE tiket_teknisi.NAMA = '18970231' AND tiket_teknisi.status = 'Close'");
        $teknisi13Count = mysqli_num_rows($teknisi13);

        $teknisi14 = mysqli_query($conn,"SELECT tiket_teknisi.status FROM tiket_teknisi INNER JOIN t_semua ON tiket_teknisi.id_incident = t_semua.id_incident WHERE tiket_teknisi.NAMA = '18960224' AND tiket_teknisi.status = 'Close'");
        $teknisi14Count = mysqli_num_rows($teknisi14);

        $teknisi15 = mysqli_query($conn,"SELECT tiket_teknisi.status FROM tiket_teknisi INNER JOIN t_semua ON tiket_teknisi.id_incident = t_semua.id_incident WHERE tiket_teknisi.NAMA = '18940292' AND tiket_teknisi.status = 'Close'");
        $teknisi15Count = mysqli_num_rows($teknisi15);
    
    ?>

    <script type='text/javascript'>

        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Status');
            data.addColumn('number', 'Jumlah');
            data.addRows([
                ['Open', <?php echo $openCount?>],
                ['Pending', <?php echo $pendingCount?>],
                ['Close', <?php echo $closeCount?>]
            ]);

            var pcDaerah = new google.visualization.DataTable();
            pcDaerah.addColumn('string', 'Daerah');
            pcDaerah.addColumn('number', 'Jumlah');
            pcDaerah.addRows([
                ['Magelang', <?php echo $magelangCount?>],
                ['Gombong', <?php echo $gombongCount?>],
                ['Karanganyar', <?php echo $karanganyarCount?>],
                ['Mertoyudan', <?php echo $mertoyudanCount?>],
                ['Kebumen', <?php echo $kebumenCount?>],
                ['Sawitan', <?php echo $sawitanCount?>],
                ['Kutowinangun', <?php echo $kutowinangunCount?>],
                ['Muntilan', <?php echo $muntilanCount?>],
                ['Kutoarjo', <?php echo $kutoarjoCount?>],
                ['Parakan', <?php echo $parakanCount?>],
                ['Purworejo', <?php echo $purworejoCount?>],
                ['Temanggung', <?php echo $temanggungCount?>],
                ['Wonosobo', <?php echo $wonosoboCount?>]
            ]);

            var dataColumnChart = google.visualization.arrayToDataTable([
                ['Status', 'Jumlah', { role: 'style' }],
                ['Open', <?php echo $openCount?>, 'blue'],            // RGB value
                ['Pending', <?php echo $pendingCount?>, 'red'],            // English color name
                ['Close', <?php echo $closeCount?>, 'yellow'],

            // ['Platinum', 21.45, 'color: #e5e4e2' ], // CSS-style declaration
            ]);

            var chartColumnTeknisi = google.visualization.arrayToDataTable([
                ['Status', 'Jumlah', { role: 'style' }],
                ['Archan Gusta Raynaldo', <?php echo $teknisi1Count?>, 'blue'],
                ['Setiawan Hardiansyah Pribadi', <?php echo $teknisi2Count?>, 'red'],
                ['Faza Chiyarurohman', <?php echo $teknisi3Count?>, 'yellow'],
                ['Wahyu Nugroho', <?php echo $teknisi4Count?>, 'green'],
                ['Muhammad Hendra Gunawan', <?php echo $teknisi5Count?>,'pink'],
                ['Muh Ikhsan Alfaruk', <?php echo $teknisi6Count?>,'black'],
                ['Syaifudin', <?php echo $teknisi7Count?>,'grey'],
                ['Arif Priyo Wibowo', <?php echo $teknisi8Count?>,'brown'],
                ['Angga Dewanto', <?php echo $teknisi9Count?>,'cyan'],
                ['Ryan Anggara', <?php echo $teknisi10Count?>,'orange'],
                ['Nur Cholis Hidayat', <?php echo $teknisi11Count?>,'#42f4d4'],
                ['Yordan Yavani', <?php echo $teknisi12Count?>,'#c141f4'],
                ['Hermawanto', <?php echo $teknisi13Count?>,'#e5f4424'],
                ['Rusman Ndani', <?php echo $teknisi14Count?>,'#d3f441'],
                ['Moses Roberto', <?php echo $teknisi15Count?>,'#d3a771']
            // ['Platinum', 21.45, 'color: #e5e4e2' ], // CSS-style declaration
            ]);

            // Set chart options
            var options = {'title':'Data Tiket <?php echo $thisDateMonth ?> ',
                            'width':500,
                            'height':300};
            var optionsDaerah = {'title':'Data Tiket Daerah <?php echo $thisDateMonth ?>',
                            'width':500,
                            'height':300};
            var optionColumnTeknisi = {'title':'Data Tiket Close Teknisi',
                    'width':900,
                    'height':300}; 
                            

            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('pie_chart_div'));
            var chartDaerah = new google.visualization.PieChart(document.getElementById('pc_daerah'));
            var chartColumn = new google.visualization.ColumnChart(document.getElementById('column_chart_div'));
            var chartTeknisi = new google.visualization.ColumnChart(document.getElementById('columnTeknisi'));
            
            chart.draw(data, options);
            chartDaerah.draw(pcDaerah, optionsDaerah);
            chartColumn.draw(dataColumnChart, options);
            chartTeknisi.draw(chartColumnTeknisi, optionColumnTeknisi);
        }

</script>


</head>
<body>

    <div class="canvas">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="informationtiket.php">Statistik Tiket</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="../index.php">Home</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="informationtiket.php">Tiket by Bulan</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="informationdaerah.php">Tiket Daerah</a>
        </li>
         <li class="nav-item">
            <a class="nav-link" href="informationteknisi.php">Tiket Teknisi</a>
        </li>
        </ul>
    </div>
    </nav>

    <form action="informationtiket.php" method="post">
        <div class="input-group mb-3">
        <div class="input-group-prepend">
            <label class="input-group-text" for="inputGroupSelect01">Show By : </label>
        </div>
        <select name = "type_bulan" class="custom-select" id="infTiket">
            <option selected>Pilih . . .</option>
            <option value="01">Januari</option>
            <option value="02">Februari</option>
            <option value="03">Maret</option>
            <option value="04">April</option>
            <option value="05">Mei</option>
            <option value="06">Juni</option>
            <option value="07">Juli</option>
            <option value="08">Agustus</option>
            <option value="09">September</option>
            <option value="10">Oktober</option>
            <option value="11">November</option>
            <option value="12">Desember</option>
        </select>
        <button type="submit" class="btn btn-default">Submit</button>
        </div> 
    </form>

    <!--Div that will hold the pie chart-->
   <table>
    <tr>
    <th><div class="posisibulanini" id="pie_chart_div"></div></th>
    </tr>
    <tr>
    <th><div class="posisibulanini" id="column_chart_div"></div></th>
    </tr>
    <tr>
    <th><div class="posisibulanini" id="pc_daerah"></div></th>
    </tr>
    <tr>
    <th><div class="posisibulanini" id="cc_daerah"></div></th>
    </tr>
    <tr>
    <th><div class="posisi1" id="columnTeknisi"></div></th>
    </tr>
    </table>

    
    
    </div>
</body>
</html>