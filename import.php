<?php 
    include "inc/koneksi.php";
?>
<fieldset>
<div class="import">
    <legend id="tulisanimport">Import</legend>

<?php
    require "excel_reader.php";

    if(isset($_POST['submit'])){
        
        $target = basename($_FILES['filePelanggan']['name']);
        move_uploaded_file($_FILES['filePelanggan']['tmp_name'],$target);

        // $data = new Spreadsheet_Excel_Reader($_FILES['filePelanggan']['name'],false);
        $data = new Spreadsheet_Excel_Reader($_FILES['filePelanggan']['name'],false);
        $sql=null;
        $baris = $data -> rowcount($sheet_index=0);

        for($i=2; $i<=$baris; $i++){
            $id_incident        = $data->val($i,1);
            $status             = $data->val($i,2);
            $nama_pelanggan     = $data->val($i,3);
            $nmr_internet       = $data->val($i,4);
            $no_telepon         = $data->val($i,5);
            $alamat             = $data->val($i,6);
            $contact_person     = $data->val($i,7);
            $tanggal            = $data->val($i,8);
            $tanggal_close      = $data->val($i,9);
            $tx                 = $data->val($i,10);
            $rx                 = $data->val($i,11);
            $closed_by          = $data->val($i,12);
            $k_kendala          = $data->val($i,13);
            $k_closed           = $data->val($i,14);
            $material           = $data->val($i,15);
            $teknisi            = $data->val($i,16);
            $kantor             = $data->val($i,17);
            
            $sql= mysqli_query($conn,"INSERT into t_semua (id_incident,status,nmr_internet,nama_pelanggan,no_telepon,alamat,contact_person,tanggal_open,tanggal_close,tx,rx,closed_by,k_kendala,k_closed,material,teknisi) values ('$id_incident','$status','$nmr_internet','$nama_pelanggan','$no_telepon','$alamat','$contact_person','$tanggal','$tanggal_close','$tx','$rx','$closed_by','$k_kendala','$k_closed','$material','$teknisi')") or die(mysqli_error($conn));;
            $sql1 = mysqli_query($conn,"INSERT into t_daerah (kantor,id_incident) values ('$kantor','$id_incident')");
            $sql2 = mysqli_query($conn, "INSERT INTO tiket_teknisi (nama, id_incident, status) values ('$teknisi', '$id_incident', '$status')");
        }

        if(!$sql && !$sql1){
            die(mysqli_error($conn));
        
        }else{
            echo "Data berhasil di Impor";
        }

        unlink($_FILES['filePelanggan']['name']);
    }
?>

<form name="myForm" id="myForm" onSubmit="return validateForm()" action="index.php?page=import" method="post" enctype="multipart/form-data">
    <input class="import-style" type="file" name="filePelanggan"/>
    <input class="submit1" type="submit" name="submit" value="Import"/><br/>
</form>
</div>
</fieldset>


<script type="text/javascript">

    function validateForm(){
        function hasExtension(inputID, exts){
            var fileName= document.getElementById(inputID).value;
            return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.')+ ')$')).test(fileName);
        }
        if(!hasExtension('filePelanggan', ['.xls'])){
            alert("Hanya file XLS (Excel 2003) yang diijinkan.");
            return false;
        }
    }

</script>