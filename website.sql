-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 30, 2018 at 09:49 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `website`
--

-- --------------------------------------------------------

--
-- Table structure for table `tiket_teknisi`
--

CREATE TABLE `tiket_teknisi` (
  `NIK` int(16) NOT NULL,
  `NAMA` varchar(255) NOT NULL,
  `id_incident` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tiket_teknisi`
--

INSERT INTO `tiket_teknisi` (`NIK`, `NAMA`, `id_incident`, `status`) VALUES
(0, '96170188', '1', 'Open'),
(0, '96170188', '10', 'Pending'),
(0, '96170188', '11', 'Pending'),
(0, '96170188', '12', 'Pending'),
(0, '96170188', '13', 'Pending'),
(0, '96170188', '14', 'Pending'),
(0, '96170188', '15', 'Pending'),
(0, '96170188', '16', 'Pending'),
(0, '96170188', '17', 'Pending'),
(0, '97170297', '18', 'Open'),
(0, '97170297', '19', 'Open'),
(0, '96170188', '2', 'Close'),
(0, '97170297', '20', 'Open'),
(0, '97170297', '21', 'Open'),
(0, '97170297', '22', 'Open'),
(0, '97170297', '23', 'Open'),
(0, '97170297', '24', 'Close'),
(0, '97170297', '25', 'Close'),
(0, '97170297', '26', 'Close'),
(0, '97170297', '27', 'Close'),
(0, '97170297', '28', 'Close'),
(0, '97170297', '29', 'Close'),
(0, '96170188', '3', 'Pending'),
(0, '97170297', '30', 'Close'),
(0, '97170297', '31', 'Close'),
(0, '96170188', '4', 'Pending'),
(0, '97170297', '4444', 'Pending'),
(0, '96170188', '5', 'Pending'),
(0, '97170297', '5555', 'Pending'),
(0, '96170188', '6', 'Pending'),
(0, '97170297', '6666', 'Pending'),
(0, '96170188', '7', 'Pending'),
(0, '97170297', '7777', 'Pending'),
(0, '96170188', '8', 'Pending'),
(0, '96170188', '9', 'Pending');

-- --------------------------------------------------------

--
-- Table structure for table `t_daerah`
--

CREATE TABLE `t_daerah` (
  `kantor` varchar(50) NOT NULL,
  `id_incident` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_daerah`
--

INSERT INTO `t_daerah` (`kantor`, `id_incident`) VALUES
('Temanggung', '1'),
('Magelang', '10'),
('Gombong', '11'),
('Gombong', '12'),
('Gombong', '13'),
('Gombong', '14'),
('Gombong', '15'),
('Gombong', '16'),
('Gombong', '17'),
('Gombong', '18'),
('Gombong', '19'),
('Magelang', '2'),
('Gombong', '20'),
('Gombong', '21'),
('Gombong', '22'),
('Gombong', '23'),
('Gombong', '24'),
('Muntilan', '25'),
('Muntilan', '26'),
('Muntilan', '27'),
('Muntilan', '28'),
('Muntilan', '29'),
('Magelang', '3'),
('Muntilan', '30'),
('Muntilan', '31'),
('Magelang', '4'),
('Gombong', '4444'),
('Magelang', '5'),
('Gombong', '5555'),
('Magelang', '6'),
('Gombong', '6666'),
('Magelang', '7'),
('Gombong', '7777'),
('Magelang', '8'),
('Magelang', '9');

-- --------------------------------------------------------

--
-- Table structure for table `t_login`
--

CREATE TABLE `t_login` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `pass` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_login`
--

INSERT INTO `t_login` (`id`, `username`, `pass`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `t_semua`
--

CREATE TABLE `t_semua` (
  `id_incident` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL,
  `nmr_internet` varchar(90) NOT NULL,
  `nama_pelanggan` varchar(50) NOT NULL,
  `no_telepon` varchar(20) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `contact_person` varchar(50) NOT NULL,
  `tanggal_open` varchar(50) NOT NULL,
  `tanggal_close` varchar(50) NOT NULL,
  `tx` int(50) NOT NULL,
  `rx` int(50) NOT NULL,
  `closed_by` varchar(255) NOT NULL,
  `k_kendala` varchar(255) NOT NULL,
  `k_closed` varchar(255) NOT NULL,
  `material` varchar(255) NOT NULL,
  `teknisi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_semua`
--

INSERT INTO `t_semua` (`id_incident`, `status`, `nmr_internet`, `nama_pelanggan`, `no_telepon`, `alamat`, `contact_person`, `tanggal_open`, `tanggal_close`, `tx`, `rx`, `closed_by`, `k_kendala`, `k_closed`, `material`, `teknisi`) VALUES
('1', 'Open', '1', '1', '1', '1', '1', '2018-11-30', '', 1, 1, '-', 'Pelanggan Menolak', '-', 'Splice Ulang', '96170188'),
('10', 'Pending', '10', '10', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Cuaca', '-', 'Splice Ulang', '96170188'),
('11', 'Pending', '11', '11', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Cuaca', '-', 'Splice Ulang', '96170188'),
('12', 'Pending', '12', '12', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Ukuran Balik Jelek', '-', 'Splice Ulang', '96170188'),
('13', 'Pending', '13', '13', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Splicer', '-', 'Splice Ulang', '96170188'),
('14', 'Pending', '14', '14', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Splicer', '-', 'Splice Ulang', '96170188'),
('15', 'Pending', '15', '15', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Splicer', '-', 'Splice Ulang', '96170188'),
('16', 'Pending', '16', '16', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Splicer', '-', 'Splice Ulang', '96170188'),
('17', 'Pending', '17', '17', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Splicer', '-', 'Splice Ulang', '96170188'),
('18', 'Open', '18', '17', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Tarikan Jauh', '-', 'Tarik Ulang', '97170297'),
('19', 'Open', '19', '17', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Tarikan Jauh', '-', 'Tarik Ulang', '97170297'),
('2', 'Close', '2', '2', '2', '2', '2', '2018-11-30', '', 2, 2, '-', 'Ukuran Balik Jelek', '-', 'Ganti Adaptor Conn', '96170188'),
('20', 'Open', '20', '17', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Tarikan Jauh', '-', 'Tarik Ulang', '97170297'),
('21', 'Open', '21', '17', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Tarikan Jauh', '-', 'Tarik Ulang', '97170297'),
('22', 'Open', '22', '17', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Tarikan Jauh', '-', 'Tarik Ulang', '97170297'),
('23', 'Open', '23', '17', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Ada Passive', '-', 'Ukur Massal', '97170297'),
('24', 'Close', '24', '17', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Ada Passive', '-', 'Ukur Massal', '97170297'),
('25', 'Close', '25', '17', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Ada Passive', '-', 'Ukur Massal', '97170297'),
('26', 'Close', '26', '17', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Ada Passive', '-', 'Ukur Massal', '97170297'),
('27', 'Close', '27', '17', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Ada Passive', '-', 'Ukur Massal', '97170297'),
('28', 'Close', '28', '17', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Ukuran Balik Jelek', '-', 'Konfigurasi Ulang', '97170297'),
('29', 'Close', '29', '17', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Ukuran Balik Jelek', '-', 'Konfigurasi Ulang', '97170297'),
('30', 'Close', '30', '17', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Ukuran Balik Jelek', '-', 'Konfigurasi Ulang', '97170297'),
('31', 'Close', '31', '17', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Ukuran Balik Jelek', '-', 'Konfigurasi Ulang', '97170297'),
('4', 'Pending', '4', '4', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Ukuran Balik Jelek', '-', 'Splice Ulang', '96170188'),
('4444', 'Pending', '1251251', '17', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Tarikan Jauh', '-', 'Tarik Ulang', '97170297'),
('5', 'Pending', '5', '5', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Ukuran Balik Jelek', '-', 'Perapihan Kabel', '96170188'),
('5555', 'Pending', '663636', '17', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Tarikan Jauh', '-', 'Tarik Ulang', '97170297'),
('6', 'Pending', '6', '6', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Ukuran Balik Jelek', '-', 'Perapihan Kabel', '96170188'),
('6666', 'Pending', '7474747', '17', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Tarikan Jauh', '-', 'Tarik Ulang', '97170297'),
('7', 'Pending', '7', '7', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Ukuran Balik Jelek', '-', 'Perapihan Kabel', '96170188'),
('7777', 'Pending', '458458458', '17', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Tarikan Jauh', '-', 'Tarik Ulang', '97170297'),
('8', 'Pending', '8', '8', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Cuaca', '-', 'Perapihan Kabel', '96170188'),
('9', 'Pending', '9', '9', '3', '3', '3', '2018-11-30', '', 3, 3, '-', 'Cuaca', '-', 'Perapihan Kabel', '96170188');

-- --------------------------------------------------------

--
-- Table structure for table `t_teknisi`
--

CREATE TABLE `t_teknisi` (
  `nama` varchar(255) NOT NULL,
  `nik` varchar(255) NOT NULL,
  `hak` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_teknisi`
--

INSERT INTO `t_teknisi` (`nama`, `nik`, `hak`) VALUES
('Yoel Putra', '1234578', 'Admin'),
('Ryan Anggara', '17960866', 'Teknisi'),
('Nur Cholis Hidayat', '18910073', 'Teknisi'),
('Yordan Yavani', '18920069', 'Teknisi'),
('Moses Roberto', '18940292', 'Teknisi'),
('Rusman Ndani', '18960224', 'Teknisi'),
('Hermawanto', '18970231', 'Teknisi'),
('Kuncoro Sarwo Hutomo', '20180135', 'Admin'),
('Anung Susmono Rahayu', '835880', 'Monitor'),
('Sumarno Hadi', '840063', 'Admin'),
('Gigih Rikhanji Putra Pamungkas', '87132121', 'Monitor'),
('Angga Dewanto', '87157541', 'Teknisi'),
('Arif Priyo Wibowo', '90157544', 'Teknisi'),
('Syaifudin', '91157362', 'Teknisi'),
('Muh Ikhsan Alfaruk', '93155481', 'Teknisi'),
('Muhammad Hendra Gunawan', '93157529', 'Teknisi'),
('Wahyu Nugroho', '96170188', 'Teknisi'),
('Faza Chiyarurohman', '97170294', 'Teknisi'),
('Setiawan Hardiansyah Pribadi', '97170297', 'Teknisi'),
('Archan Gusta Raynaldo', '99170157', 'Teknisi'),
('Nurul Fitriyani', 'NF290685', 'Admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tiket_teknisi`
--
ALTER TABLE `tiket_teknisi`
  ADD PRIMARY KEY (`id_incident`);

--
-- Indexes for table `t_daerah`
--
ALTER TABLE `t_daerah`
  ADD PRIMARY KEY (`id_incident`);

--
-- Indexes for table `t_login`
--
ALTER TABLE `t_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_semua`
--
ALTER TABLE `t_semua`
  ADD PRIMARY KEY (`id_incident`);

--
-- Indexes for table `t_teknisi`
--
ALTER TABLE `t_teknisi`
  ADD PRIMARY KEY (`nik`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_login`
--
ALTER TABLE `t_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
