<?php

    $server     = "localhost";
    $username   = "root";
    $password   = "";
    $db_name    = "website";
    

    $conn       = mysqli_connect($server,$username,$password,$db_name);

    if(!isset($_SESSION)) 
    { 
        session_start(); 
    }
    
    if (mysqli_connect_error()){
        echo "Failed to connect MySQL: ". mysqli_connect_error();
    }
?>